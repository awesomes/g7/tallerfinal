package Proyecto.Televisor;

import javax.swing.*;
import java.util.Scanner;

public class Control extends Televisor {

    int contador_On_Off = 0;
    int getContadorMute_On_Off = 0;
    private int Power = 0;
    private int opcion_Volumen;
    private int volumen, Volumen1;
    Scanner teclado = new Scanner(System.in);

    public Control(String marca, String modelo, int canal, String entrada, String Mute, String estado, String Volumen_f) {

        super(marca, modelo, canal, entrada, Mute, estado, Volumen_f);
        this.setOpcion_Volumen(0);
        this.setVolumen(0);
    }


    public void Menu() {


        int x = 0;


        do {

            x = Integer.parseInt(JOptionPane.showInputDialog("Control \n 1.)Canal \n 2.)Source \n 3.)Mute \n 4.)Volumen \n 5.)Power  "));

            switch (x) {

//Canal funcion
                case 1: {

                    boton_Canal();


                    break;
                }


//Source funcion
                case 2: {
                    boton_Source();


                    break;
                }
//Mute funcion
                case 3: {
                    boton_Mute();

                    break;
                }
//Volumen funcion
                case 4: {

                    boton_volumen();

                    break;
                }
//Power funcion
                case 5: {

                    boton_Power();

                    x = 6;

                    break;
                }
                default: {

                    break;
                }


            }

        }
        while (x != 6);

    }


//Metodo boton_volumen()

    public void boton_volumen() {


        do {


            setVolumen(Integer.parseInt(getVolumen_f()));

            setOpcion_Volumen(Integer.parseInt(JOptionPane.showInputDialog("EL volumen se encuentra en " + getVolumen() + "\n\nDesea ingresar el volumen (1) , sumarle al existente (2), restarle al existente (3) o volver al menu anterior(4)")));

            switch (getOpcion_Volumen()) {


                case 1: {


                    setVolumen(Integer.parseInt(JOptionPane.showInputDialog("El volumen se encuentra en " + getVolumen() + " ingrese el volumen")));

                    if (getVolumen() >= 100) {

                        setVolumen(100);

                        setVolumen_f(Integer.toString(getVolumen()));


                        setMute("");
                        break;
                    } else {
                        setVolumen_f(Integer.toString(getVolumen()));


                        setMute("");
                        break;

                    }

                }


                case 2: {


                    Volumen1 = Integer.parseInt(getVolumen_f());

                    Volumen1 += Integer.parseInt(JOptionPane.showInputDialog("El volumen se encuentra en " + getVolumen() + " ingrese las unidades que desea subirle"));

                    if (Volumen1 + getVolumen() >= 100) {
                        Volumen1 = 100;

                        setVolumen(Volumen1);
                        setVolumen_f(Integer.toString(getVolumen()));
                        setMute("");
                        break;
                    }

                    else{

                        setVolumen(Volumen1);
                        setVolumen_f(Integer.toString(getVolumen()));
                        setMute("");

                        break;
                    }



                }


                case 3: {


                    if (0 < getVolumen()) {
                        Volumen1 = Integer.parseInt(getVolumen_f());
                        Volumen1 -= Integer.parseInt(JOptionPane.showInputDialog("El volumen se encuentra en " + getVolumen() + " ingrese las unidades que desea bajarle"));

                        if (Volumen1 < 0) {
                            Volumen1 = 0;

                            setVolumen(Volumen1);
                            setVolumen_f(Integer.toString(getVolumen()));
                            setMute("");
                            break;
                        }

                        if(0<=Volumen1 ){

                            setVolumen(Volumen1);
                            setVolumen_f(Integer.toString(getVolumen()));
                            setMute("");
                            break;

                        }


                    }

                    else {

                        JOptionPane.showMessageDialog(null, "El volumen ya es cero", "Error!", JOptionPane.ERROR_MESSAGE);
                        break;
                    }


                }


                default: {
                    if (getOpcion_Volumen() == 4) {
                        break;
                    } else {

                        JOptionPane.showMessageDialog(null, "Por favor ingrese una opcion valida ", "Error!", JOptionPane.ERROR_MESSAGE);
                        break;

                    }
                }
            }
        }

        while (getOpcion_Volumen() != 4);
        Interfaz();

    }

    //Fin boton_Volumen()


    //Metodo boton_Power()
    public void boton_Power() {


        if (contador_On_Off == 1) {

            do {
                JOptionPane.showMessageDialog(null, "Desea apagar la TV?", "Control", JOptionPane.INFORMATION_MESSAGE);
                setPower(Integer.parseInt(JOptionPane.showInputDialog("1.) Si / 2.)No")));

                switch (getPower()) {

                    case 1: {

                        contador_On_Off = 0;
                        Inicio();
                        setPower(3);
                        break;
                    }


                    default: {
                        if (getPower() == 2) {
                            Interfaz();
                            Menu();

                            break;
                        } else {
                            JOptionPane.showMessageDialog(null, "Por favor ingrese una opcion valida ", "Error!", JOptionPane.ERROR_MESSAGE);
                            break;
                        }
                    }

                }


            }
            while (getPower() == 4);

        } else {
            if (contador_On_Off == 0) {

                do {
                    JOptionPane.showMessageDialog(null, "Desea encender la TV?", "Control", JOptionPane.INFORMATION_MESSAGE);
                    setPower(Integer.parseInt(JOptionPane.showInputDialog("1.) Si / 2.)No")));

                    switch (getPower()) {

                        case 1: {

                            contador_On_Off = 1;
                            Interfaz();
                            break;
                        }


                        default: {
                            if (getPower() == 2) {
                                setPower(3);

                                break;
                            } else {
                                JOptionPane.showMessageDialog(null, "Por favor ingrese una opcion valida ", "Error!", JOptionPane.ERROR_MESSAGE);
                                break;
                            }
                        }

                    }


                }
                while (getPower() == 2);
            }

        }

    }

    //Fin boton_Power()


    //Metodo boton_Source()

    public void boton_Source() {


        int Source = 0;

        do {
            Source = Integer.parseInt(JOptionPane.showInputDialog("Elija la entrada\n\n 1.) TV CABLE / 2.)HDMI / 3.)VIDEO / 4.)Cancelar"));


            switch (Source) {


                case 1: {
                    setEntrada("TV CABLE");
                    Interfaz();

                    break;
                }

                case 2: {

                    setEntrada("HDMI");

                    Interfaz();
                    break;
                }
                case 3: {

                    setEntrada("VIDEO");

                    Interfaz();
                    break;
                }

                default: {
                    if (Source == 4) {

                        break;
                    } else {
                        JOptionPane.showMessageDialog(null, "Por favor ingrese una opcion valida ", "Error!", JOptionPane.ERROR_MESSAGE);
                        break;
                    }

                }


            }


        }
        while (Source == 4);


    }

    //Fin boton_Source()

//Metodo boton_Canal()

    public void boton_Canal() {

        int Canal = 0;

        setCanal(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el canal que desea visualizar")));

        Interfaz();


    }
//Fin boton_Canal()


//Metodo boton_Mute()

    public void boton_Mute() {

        setMute("ON");
        setVolumen_f("0");

        Interfaz();


    }


    public int getOpcion_Volumen() {
        return opcion_Volumen;
    }

    public void setOpcion_Volumen(int opcion_Volumen) {
        this.opcion_Volumen = opcion_Volumen;
    }


    public int getVolumen() {
        return volumen;
    }

    public void setVolumen(int volumen) {
        this.volumen = volumen;
    }

    public int getPower() {
        return Power;
    }

    public void setPower(int power) {
        Power = power;
    }
}

