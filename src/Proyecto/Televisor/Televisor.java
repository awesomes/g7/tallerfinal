package Proyecto.Televisor;

import javax.swing.*;

public class Televisor {

    private String marca;
    private String modelo;
    private int canal;
    private String entrada;
    private String Mute;
    private String estado;
    private String Volumen_f;

    public Televisor(String marca, String modelo, int canal, String entrada, String Mute, String estado, String volumen) {
        this.setMarca("Uninpahu");
        this.setModelo("2019");
        this.setCanal(0);
        this.setEntrada("TV CABLE");
        this.setMute("");
        this.setEstado("");
        this.setVolumen_f("0");
    }



    public void Inicio (){
        setEstado("Off");
        JOptionPane.showMessageDialog(null,"Power: "+getEstado(),"Television   "+"     Marca:"+getMarca()+"    Modelo:"+getModelo(),JOptionPane.INFORMATION_MESSAGE);
    }


    public void Interfaz ()
    {
        setEstado("On");

        JOptionPane.showMessageDialog(null, "Channel: " +getCanal()+ "      Source:"+getEntrada() + "     Power:"+ getEstado() +"  \n\n\n\n"+"Volumen: "+getVolumen_f()+"     Mute:"+getMute(), "Television   "+"Modelo   "+getModelo()+"   Marca    "+getMarca(), JOptionPane.INFORMATION_MESSAGE);

        //
//        System.out.println("*****************************");
        //      System.out.println("Marca "+getMarca() +"        Modelo "+ getModelo());
        //    System.out.println("Canal "+getCanal() +"        Source "+ getEntrada() +"\n");
        //  System.out.println(""+getMute() +"        "+ getEstado() +"\n\n");
        //System.out.println("*Volumen ="+ getVolumen_f() +"*\n");
        //System.out.println("******************************\n");

    }


    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCanal() {
        return canal;
    }

    public void setCanal(int canal) {
        this.canal = canal;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getMute() {
        return Mute;
    }

    public void setMute(String Mute) {
        this.Mute = Mute;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public String getVolumen_f() {
        return Volumen_f;
    }

    public void setVolumen_f(String volumen_f) {
        Volumen_f = volumen_f;
    }
}




