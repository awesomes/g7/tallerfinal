package Proyecto.Loteria;
import jdk.swing.interop.SwingInterOpUtils;

import java.util.Random;
import java.util.Scanner;

public class Jugador{
    Random random = new Random();
    Scanner teclado = new Scanner(System.in);
    Empresa empresa =new Empresa();

    String nombres;
    int dinero = Integer.parseInt(String.format("%04d", random.nextInt(680000)));
    String apellidos;
    String ccJugador ;
    String misNumeros[] = new String[5];

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setCcJugador(String ccJugador) {
        this.ccJugador = ccJugador;
    }



    public String getNombres() {
        return nombres;
    }


    public String getApellidos() {
        return apellidos;
    }

    public String getCcJugador() {
        return ccJugador;
    }

    public void Jugar(){

        Juego juego = new Juego();
        int opcionMenuJugar=0;
        String tipoJuego="";
        int ticketComprado=0;
        System.out.println("Software Loteria");
        System.out.println("Ingrese su nombre");
        nombres = teclado.nextLine().toString();
        System.out.println("Ingrese su apellido");
        apellidos = teclado.nextLine().toString();
        System.out.println("Ingrese su numero de identificacion");
        ccJugador = teclado.nextLine().toString();


        while ((opcionMenuJugar!=3)&& (ticketComprado<=4)){

            System.out.println("");
            System.out.println("Tiene $" + dinero +" en su billetera");
            System.out.println("");
            System.out.println("Presione 1 para comprar un boleto generado automaticamente");
            System.out.println("Presione 2 para comprar un boleto generado manualmente");
            System.out.println("Presione 3 para Avanzar jugar la loteria");
            opcionMenuJugar = teclado.nextInt();

            switch (opcionMenuJugar){

                case 1: tipoJuego="automatico";
                    break;
                case 2: tipoJuego="manual";
                    break;
                case 3: empresa.iniciarJuego();
                default:
                    System.out.println("Digito una opcion no valida, el ticket se generara modo automatico");
                    tipoJuego="automatico";



            }

            juego.generarTicket(tipoJuego,nombres,apellidos,ccJugador);
            System.out.println("");
            misNumeros[ticketComprado] = juego.idTicket.toString();
            dinero=dinero-5000;
            empresa.saldo =empresa.saldo+5000;
            ticketComprado=ticketComprado+1;

            if (ticketComprado==5){
                System.out.println("No puede comprar mas tickets, empezara el juego de la loteria en 3.2.1..");
                System.out.println("");
            }

        }

    }

    public void reclamarPremio(){
        int tempSaldo= dinero;
        empresa.iniciarJuego();

        for (int i = 0;i<misNumeros.length-1;i++){

           if (Integer.parseInt(misNumeros[i].toString())==Integer.parseInt(empresa.numeroGanador)){
                dinero = dinero + 10000000;
                System.out.println("!!!GANASTE CON EL NUMERO "+misNumeros[i]);
            }

        }
        if (dinero==tempSaldo){
            System.out.println("Tu numero no cayo :( , buena suerte para la proxima");
        }
        System.out.println("");
        System.out.println("Tu saldo anterior $"+tempSaldo);
        System.out.println("Ganancias $"+ (dinero -tempSaldo) );
        System.out.println("Tu saldo actual $"+dinero);
        empresa.entregarDinero((dinero -tempSaldo));

    }

}