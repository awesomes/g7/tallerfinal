package Proyecto.Loteria;

import java.util.Random;
import java.util.Scanner;

public class Empresa {
    Random random = new Random();
    Scanner teclado = new Scanner(System.in);
    String numeroGanador;
    Integer saldo=Integer.parseInt(String.format("%04d", random.nextInt(100000000)));


    private String nombre = "Uninpahu";
    private String nit = "800135567-2";


    public String getNombre() {
        return nombre;
    }

    public String getNit() {
        return nit;
    }

    public void iniciarJuego(){
        System.out.println("****************************");
        System.out.println("Inicia el sorteo ");
        System.out.println("__________________");
        System.out.print("El numero ganador es: ");
        //PARA PRUEBAS DE GANAR LOTERIA QUITAR LA ETIQUETA DE COMENTARIO DE LA SIGUIENTE LINEA Y COMENTAR LA DE ABAJO
        //numeroGanador ="2043";
        numeroGanador=String.format("%04d", random.nextInt(1000));

        System.out.println(numeroGanador);
        System.out.println("__________________");


    }

    public void entregarDinero(Integer valorAEntregar){
        System.out.println("");
        System.out.println("");
        System.out.println("Saldo anterior de la empresa: $"+saldo);
        saldo=saldo-valorAEntregar;
        System.out.println("Saldo actual de la empresa: $"+saldo);
        System.out.println("");
        System.out.println("Loteria Finalizada, presione una tecla para continuar");
        teclado.nextLine();

    }
}
