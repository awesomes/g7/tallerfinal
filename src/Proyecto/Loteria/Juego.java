package Proyecto.Loteria;

import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;


public class Juego extends Empresa {


    public String getIdTicket() {
        return idTicket;
    }

    public String idTicket;


    Scanner teclado = new Scanner(System.in);

    //genera el volante de sorteo para el jugador

    public void generarTicket(String tipo,  String nombre, String apellido, String ccJugador) {

        Calendar fecha = Calendar.getInstance();
        Random random = new Random();
        idTicket = "";
        int ticketValido=0;
        //random
        if (tipo == "automatico"){
            idTicket = String.format("%04d", random.nextInt(1000));
        }
        //manual
        else if (tipo == "manual"){

            while (ticketValido==0){
                System.out.println("Por favor digite 4 numeros para generar su ticket de loteria ");
                idTicket = teclado.nextLine().toString();
                //validar numero valido 
                if ((Integer.parseInt(idTicket)<1000)||(Integer.parseInt(idTicket)>9999)){
                    System.out.println("El numero ingresado no es valido, intente nuevamente");

                }else{
                    System.out.println("Ticket Generado Satisfactoriamente");
                    ticketValido=1;
                }

            }
        }


        System.out.println(  "----------------------------"+"\n" +
                "Loteria de " + super.getNombre().toString()+ "\n" +
                "Nit: "+super.getNit().toString()+  "\n" +
                "Fecha Compra: " + fecha.getTime().toString()+ "\n"+
                "Tu numero: "+ idTicket+"\n"+
                "----------------------------");

    }
}